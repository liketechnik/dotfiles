runtime! plugins/init.vim
runtime! configs/*.vim

" set list " display tabs and trailing spaces

set backup " keep a backup copy when overwriting

set history=100 " 100 items in history

set signcolumn=auto:3 " always show sign column
