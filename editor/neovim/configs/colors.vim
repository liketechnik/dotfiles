" Color settings
set termguicolors

set background=dark

colorscheme gruvbox

highlight Normal ctermbg=NONE guibg=NONE " must come last
