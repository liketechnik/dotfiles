" toggle file tree
nnoremap <Space>ff :NERDTreeToggle<CR>

" window movement
nnoremap <Space>wj <C-W>j
nnoremap <Space>wk <C-W>k
nnoremap <Space>wl <C-W>l
nnoremap <Space>wh <C-W>h
nnoremap <Space>wc :close<CR>

" Buffers
nnoremap <Space>bb :Buffers<CR>
nnoremap <Space>bd :bd<CR> " close (aka delete) buffer

" Reload configs 
nnoremap <Space>rrr :source ~/.config/nvim/init.vim<CR>

" Search settings
nnoremap <Space>sc :set ignorecase!<CR> " toggle case (in)sensitive search 
nnoremap <Space>sh :nohlsearch<CR> " remove highlighting for the last search

" Opening config files 
nnoremap <Space>ec :edit ~/.config/nvim/
nnoremap <Space>esf :Rg<CR>
nnoremap <Space>esr :RgR<CR>
nnoremap <Space>ef :Files<CR>
nnoremap <Space>eg :GFiles<CR>

" Exit 
nnoremap <Space>q :q<CR>
nnoremap <Space>w :w<CR>
nnoremap <Space>wq :wq<CR>
nnoremap <Space>qq :qa<CR>


" Swapping two windows
function! WinBufSwap()
	let thiswin = winnr()
	let thisbuf = bufnr("%")
	let lastwin = winnr("#")
	let lastbuf = winbufnr(lastwin)
	exec lastwin . " wincmd w" ."|".
		\ "buffer ". thisbuf ."|".
		\ thiswin ." wincmd w" ."|".
		\ "buffer ". lastbuf
endfunc

command! Wswap :call WinBufSwap()
map <Silent> <Space>ws :call WinBufSwap()<CR>

" format after putting
function! FormatPut()
	let curLine = line(".")
	execute "normal gp"
	let afterPasteLine = line(".")
	let difference = afterPasteLine - curLine
	exec "normal " curLine . "G" 	
	if difference > 0
		exec "normal "difference . "=="
	endif
endfunc

nnoremap <Space>pf :call FormatPut()<CR>

" autocompletion
inoremap <expr> <C-J> pumvisible() ? "\<C-n" : "\<C-J>"
inoremap <expr> <C-K> pumvisible() ? "\<C-p" : "\<C-K>"
