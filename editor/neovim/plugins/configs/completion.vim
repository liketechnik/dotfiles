set completeopt=menuone,noinsert,noselect

set shortmess+=c

let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy', 'all']

let g:completetion_matching_smart_case = 1

" Use completion-nvim in every buffer
autocmd BufEnter * lua require'completion'.on_attach()
