lua << EOF
local custom_lsp_attach = function(client)
	local opts = {noremap = true}

	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>ch', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)	
	-- following currently broken, but known issue
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>cH', '<cmd>lua vim.lsp.buf.hover()vim.lsp.buf.hover()<CR>', opts) 
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>cn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>cd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>ct', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>crr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>crf', '<cmd>lua vim.lsp.buf.outgoing_calls()<CR>', opts)
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>cru', '<cmd>lua vim.lsp.buf.incoming_calls()<CR>', opts)
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>crs', '<cmd>lua vim.lsp.buf.workspace_symbol()<CR>', opts)
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>cf', '<cmd>lua vim.lsp.buf.formatting_sync()<CR>', opts)
	vim.api.nvim_buf_set_keymap(0, 'n', '<Space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
end

local lspconfig = require'lspconfig'
lspconfig.bashls.setup{
	on_attach = custom_lsp_attach
}
lspconfig.ccls.setup{
	root_dir = lspconfig.util.root_pattern(".ccls-root", "compile_commands.json", ".git"),
	on_attach = custom_lsp_attach
}
lspconfig.cssls.setup {
	on_attach = custom_lsp_attach
}
lspconfig.diagnosticls.setup{
	on_attach = custom_lsp_attach
}
lspconfig.dockerls.setup {
	on_attach = custom_lsp_attach
}
lspconfig.html.setup {
	on_attach = custom_lsp_attach
}
lspconfig.jedi_language_server.setup {
	on_attach = custom_lsp_attach
}
lspconfig.jsonls.setup  {
	on_attach = custom_lsp_attach
}
lspconfig.rust_analyzer.setup  {
	on_attach = custom_lsp_attach
}
lspconfig.sqlls.setup {
	cmd = { "sql-language-server", "up", "--method", "stdio" },
	on_attach = custom_lsp_attach
}
lspconfig.texlab.setup  {
	on_attach = custom_lsp_attach
}
lspconfig.vimls.setup  {
	on_attach = custom_lsp_attach
}
EOF
