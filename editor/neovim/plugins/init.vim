" Plugins will be downloaded under the specified directory.
call plug#begin('~/.config/nvim/plugged')
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'

Plug 'andymass/vim-matchup' " jump between if/else etc. instead of only ()

Plug 'vim-utils/vim-man' " Grepping Man pages
Plug 'roxma/vim-tmux-clipboard' " Syncing tmux/nvim clipboard
Plug 'overcache/NeoSolarized'  
"Plug 'vim-airline/vim-airline-themes' 
"Plug 'critiqjo/lldb.nvim' " LLDB frontend
Plug 'preservim/nerdtree' " File Tree
Plug 'Xuyuanp/nerdtree-git-plugin' 
Plug 'junegunn/fzf' " possibly add { 'do': { -> fzf#install() } } here
Plug 'junegunn/fzf.vim' " default configuration for fzf
Plug 'tpope/vim-commentary' " commenting out stuff
Plug 'bkad/CamelCaseMotion' " adds support for CamelCase and _-Notation when moving
Plug 'alvan/vim-closetag' " close html tags
Plug 'sheerun/vim-polyglot' " indenting and stuff for most languages
Plug 'tpope/vim-surround' " parentheses, brackets, quotes, XML tags, etc.
Plug 'thaerkh/vim-indentguides'

" this block is configured
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-repeat' " . (repeat) support for plugins
Plug 'airblade/vim-gitgutter' " highlighting git changes
Plug 'vim-airline/vim-airline' " status bar

" this block is new
Plug 'frazrepo/vim-rainbow' " color parentheses 
Plug 'ActivityWatch/aw-watcher-vim' " activitywatch support

Plug 'ryanoasis/vim-devicons' " Needs Nerd Font compatible font, file icons, should be last item
" List ends here. Plugins become visible to Neovim after this call.
call plug#end()

runtime! plugins/configs/*.vim
