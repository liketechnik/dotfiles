# Basic editing

## Deleting
 
* `J` removes the linebreak after the current line 

## Appending

* `a` (append) supports counts (`3a!` appends 3 exclamation marks to the current word)

## Exiting

* `ZZ` writes and saves
* `:e!` reloads the original version of the file

# Movement

## Word movement

* `w` (word) moves to the start of the next word 
* `b` (back) moves to the start of the previous word 
* `e` (end) moves to the end of the next word 
* `ge` moves to the end of the previous word 

## Move to a character 

* `fx` (find) searches forward for the character `x`
* `Fx` searches backward for `x` 
* `tx` (to) stops one character before 
* `Tx` backward version
* `;` repeat command, `,` repeats in the other direction
* `%` goes to matching paren (e. g. `(`)

## Move inside the window

* `H` (home) move to the top displayed line 
* `M` (middle) move to the middle displayed line
* `L` (last) move to the bottom displayed line 
 
## Scrolling

* `CTRL-U` (up) scroll half a screen of text (moves upwards in the file) 
* `CTRL-D` (down) scroll half a screen of text (moves downwards in the file) 
* `CTRL-E` (extra) scroll up (go down one line)
* `CTRL-Y` scroll down (up one line)
* `CTRL-F` (forward) forward by one whole screen 
* `CTRL-B` (backward) backward by one screen 
* `zz` move so the currently focussed line is in the middle of the screen 
* `zb` (bottom) cursor line to the bottom 
* `zt` (top) cursor line to the top 

## Searching

* `/` searches 
* `n` goes to the next match 
* `?` search backwards 
* `N` searches in the opposite direction 
* Case matching: `:set ignorecase` | `:set noignorecase` (`CTRL-S`) 
* `*` Search for the word under the cursor 
* `#` opposite direction 
* Special markers: `\>` (end of a word) and `\<` (start of a word) 
* `g*` match partial word 
* `g#` ..

## Marks

* ```` jump to the previous position (counts as a jump) 
* `CTRL-O` jump back to the previous position
* `:jumps" position where i jumped
* `ma`: Place mark 'a'
* ``a` or `'a`: Jump to mark 'a' 
* ``.`: Last change in that file.
* ``"`: Last position in the file.
* Use upper case letters to place global (non file specific) marks

# Making small changes 

## Changing text 

* `cw`: Change a word (or anything else, replace `w` with a move operator)

### Abbreviations 

* `X`: delete left of the cursor
* `D`: delete to end of line
* `C`: change to end of line 
* `s`: change one character 
* `S`: change a whole line 

## Selecting

* `V`: selects only whole lines 
* `CTRL-V`: select a block of text
* `o`: Goes to the other edn of your selection

## Moving text 

* `P`: like `p` but before the cursor 
* `xp`: Swap to characters 

## Copying text 

* `yw`: (Copy) Yank a word. 
* Working with the system clipboard: Prepend `"*` to all commands 

## Text objects 

* `daw`: Delete the word the cursor is in ('aw' ^= a word)
* `is`: Inner Sentence
* `as`: A sentence
* `ap`: a paragraph
* `a]`: A [] block 
* `a)`: ..
* `a<`: ..
* `a{`: ..
* `a"`: ..
* `a'`: ..

## Replace mode

* `R`: Like 'insert' mode in LibreOffice 

# Splitting windows 

* `:split`: Split the screen into two windows (optionally accepts filename to open)
* `CTRL-W``w`: Jump between windows 
* `:close`: Closes the current window.
* `:only`: Close all windows except the current one.
* `:new`: New empty window.
* `:vsplit`: Vertical split.
* `:vertical command`: Perform some split command vertically instead of horizontally.

## Movement 

* `CTRL-W` `movement key`: Move left/right/up/down 
* `CTRL-W` `uppercase movement key`: Move the current window to the top/bottom/left/right.

## Commands for all windows 

* `:qall`: Quit all. 
* `:wall`: Write all.
