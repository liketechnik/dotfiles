function ffmpeg-mkv
	if test (count $argv) -ne 2
		echo "Expecting 2 arguments!"
		return -1
	end

	# first pass
	#	ffmpeg -hwaccel auto -hwaccel_device /dev/dri/renderD128 -i $argv[1] -c:v libvpx-vp9 -crf 10 -b:v 0 -row-mt 1 -pass 1 -an -f null /dev/null

	# second pass
	#ffmpeg -hwaccel auto -hwaccel_device /dev/dri/renderD128 -i $argv[1] -c:v libvpx-vp9 -crf 10 -b:v 0 -row-mt 1 -pass 2 -c:a libopus -application voip -compression_level 10 $argv[2]
	ffmpeg -hwaccel auto -hwaccel_device /dev/dri/renderD128 -i $argv[1] -c:v libvpx-vp9 -crf 10 -b:v 0 -row-mt 1 -c:a libopus -application voip -compression_level 10 $argv[2]
end
