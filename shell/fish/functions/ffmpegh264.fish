function ffmpegh264
	if test (count $argv) -ne 2
		echo "Expecting 2 arguments!"
		return -1
	end

	ffmpeg -hwaccel vaapi -hwaccel_device /dev/dri/renderD128 -hwaccel_output_format vaapi -i $argv[1] -c:v h264_vaapi -c:a aac $argv[2]
end
