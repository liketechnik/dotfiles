function ffmpeg-ogv
	if test (count $argv) -ne 2
		echo "Expecting 2 arguments!"
		return -1
	end

	ffmpeg -hwaccel auto -hwaccel_device /dev/dri/renderD128 -i $argv[1] -c:v theora -qscale:v 6 -c:a libspeex -application voip -compression_level 0 $argv[2]
end
