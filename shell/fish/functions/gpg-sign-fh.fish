function gpg-sign-fh
	gpg2 --local-user "Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de" --armor --output $argv[1].sig --detach-sign $argv[1]
end
