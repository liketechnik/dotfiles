function edit
	if not /home/florian/bin/emacs_connect.sh $argv[1] -n $argv[2..(count $argv)]
		xdotool key --delay 0 alt+shift+e
		sleep 0.1
		xdotool type --delay 0 $argv[1]
		sleep 0.5
		xdotool key --delay 0 Return
		sleep 1.5
		/home/florian/bin/emacs_connect.sh $argv[1] -n $argv[2..(count $argv)]
	end
end
