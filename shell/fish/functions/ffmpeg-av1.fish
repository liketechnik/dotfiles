function ffmpeg-av1
	if test (count $argv) -ne 2
		echo "Expecting 2 arguments!"
		return -1
	end

	ffmpeg -hwaccel auto -hwaccel_device /dev/dri/renderD128 -i $argv[1] -c:v libaom-av1 -crf 20 -b:v 0 -row-mt 1 -c:a libspeex -row-mt 1 $argv[2]
end
