set -g theme_powerline_fonts yes
set -g theme_nerd_fonts yes
set -g theme_display_git_stashed_verbose yes
set -g theme_display_git_master_branch yes
set -g theme_display_git_untracked yes
set -g theme_display_git_dirty yes
set -g theme_display_virtualenv yes

set -g theme_title_display_user ssh
set -g theme_display_user ssh
set -g theme_display_hostname ssh
set -g theme_display_date yes
set -g theme_display_cmd_duration yes
set -g theme_title_use_abbreviated_path no
set -g theme_date_format "+%a %H:%M"
set -g theme_avoid_ambigous_glyphs yes
set -g theme_show_exit_status yes
set -g theme_display_jobs_verbose yes
#set -g theme_color_scheme solarized-dark
set -g theme_color_scheme gruvbox
set -g theme_newline_cursor clean

set -g fish_prompt_pwd_dir_length 5

alias glances="sudo glances"
alias btm="sudo btm"
alias pdflatex="pdflatex -shell-escape"
alias disable_webcam="sudo modprobe -r uvcvideo"
alias enable_webcam="sudo modprobe uvcvideo"

fish_vi_key_bindings insert

set -g SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)

set -ga fish_user_paths ~/bin ~/.cargo/bin ~/.emacs.d/bin ~/.local/bin
