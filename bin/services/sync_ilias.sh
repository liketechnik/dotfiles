#!/usr/bin/env bash
set -euo pipefail

echo "Snoozing"
# run every 4 hours
snooze -t /home/florian/.timefiles/sync_ilias -d '*' -m '*' -w '*' -H '*' -M '*' -S '*' -T 2h

echo "Checking network"
wget -q --tries=10 --timeout=20 --spider https://www.fh-bielefeld.de/elearning
if [[ $? -eq 0 ]]; then
    echo "having network connection"
    # only with working internet connection
    touch /home/florian/.timefiles/sync_ilias

    echo "export ssh auth socket"
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
    export HOME=/home/florian
    export LANG=de_DE.UTF-8

    echo "Syncing Ilias files"
    /home/florian/bin/copy_ilias.sh 2>&1

    echo "Adding all changes to git, commit & pushing"
    cd /home/florian/Documents/fh/WS3_20_21_Ilias_Kopien 2>&1
    set +e
    git add . 2>&1
    git commit -m "UPDATE: auto am `date`" 2>&1
    git push 2>&1
    set -e

    touch /home/florian/.timefiles/sync_ilias
else
    # wait 5 minutes and retry
    echo "wait 30secs, no connection"
    sleep 30
fi
