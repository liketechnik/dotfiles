#!/bin/sh
if ! tmux has -t 'terminal' 2> /dev/null ; then
    tmux new-session -d -s 'terminal' 
fi

tmux attach -t 'terminal'
