#!/bin/sh
if ! tmux has -t 'system-info' 2> /dev/null ; then
    tmux new-session -d -s 'system-info' -n 'glances' 'glances'
    tmux new-window -n 'btm' 'btm'
    tmux new-window -n 'htop' 'sudo htop'
	tmux new-window -n 'zenith' 'sudo zenith'
fi

tmux attach -t 'system-info'
