#!/usr/bin/env fish

set vpns "mullvad-de1" "mullvad-de4" "mullvad-de5" "mullvad-de6"

for vpn in $vpns
    if test "$vpn" = "$argv[1]"
        echo Turning on $argv[1]
        sudo nmcli c up $argv[1]
    else
        echo Turning off $vpn
        sudo nmcli c down $vpn
    end
end
