#!/usr/bin/bash
# --glob "*" everything
# ~/Documents/fh/Ilias search in this dir
# -d 1 search depth 1
# -t d only directories
# -x ... \; execute ... for every result
# {} inside -x replace with path
fd --glob "*" ~/Documents/fh/Ilias -d 1 -t d -x mount {} \; 
