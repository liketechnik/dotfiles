#!/usr/bin/bash
set -euo pipefail

/home/florian/bin/mount_ilias.sh
rsync -rc --exclude="lost+found" ~/Documents/fh/Ilias/ ~/Documents/fh/WS3_20_21_Ilias_Kopien
