#!/usr/bin/env bash
set -euo pipefail

export SUDO_ASKPASS="/usr/bin/gnome-ssh-askpass"
bash -c "notify-send VPN-Status '$(sudo wg)'"
