#!/usr/bin/env bash
set -euo pipefail

export SUDO_ASKPASS="/usr/bin/gnome-ssh-askpass"
bash -c "notify-send 'FH-VPN Log' '$(sudo tail /var/log/fh-vpn/current | bat)'"
