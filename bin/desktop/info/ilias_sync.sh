#!/usr/bin/env bash
set -euo pipefail

bash -c "notify-send 'Ilias-Sync Log' '$(tail /home/florian/log/ilias_sync/current | bat)'"
