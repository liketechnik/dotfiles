#!/usr/bin/env bash
set -euo pipefail

CATEGORIES="action\n
info\n
editor"

if [[ $# -eq 1 && $CATEGORIES == *"$1"* ]]; then
    CATEGORY="$1"
else
    CATEGORY="$(echo -e "${CATEGORIES}" | dmenu -i -p "Script category:")"
fi

SCRIPTS="$(ls /home/florian/bin/desktop/${CATEGORY})"

SCRIPT="$(echo "${SCRIPTS}" | dmenu -i -p "Run script:")"
bash -c "/home/florian/bin/desktop/${CATEGORY}/${SCRIPT}"
