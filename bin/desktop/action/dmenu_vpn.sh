#!/usr/bin/env bash
set -euo pipefail

# see
# https://stackoverflow.com/questions/9139401/trying-to-embed-newline-in-a-variable-in-bash#9139891
# for the linebreak hack
VPN_LIST="off\n
mullvad-de1\n
mullvad-de4\n
mullvad-de5\n
mullvad-de6"

VPN=$(echo -e ${VPN_LIST} | dmenu -i -p "Turn on VPN:")

export SUDO_ASKPASS="/usr/bin/gnome-ssh-askpass"
bash -c "sudo -A /home/florian/bin/vpn_switch.fish ${VPN}"
