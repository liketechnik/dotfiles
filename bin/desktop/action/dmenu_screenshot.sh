#!/usr/bin/env bash
set -euo pipefail

# see
# https://stackoverflow.com/questions/9139401/trying-to-embed-newline-in-a-variable-in-bash#9139891
# for the linebreak hack
DIR_LIST="~/Documents/flatpak_shared/discord/\n
~/Desktop/"

DIR=$(echo -e ${DIR_LIST} | dmenu -i -p "Save in?")
NAME=$(echo $(date +%a_%d%m-%Y_%H%M-%S).png | dmenu -i -p "Name?")

bash -c "/home/florian/bin/screengun.sh ${DIR}/${NAME}"
