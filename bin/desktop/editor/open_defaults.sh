#!/usr/bin/env bash
set -euo pipefail

# Defaults must contain an associative array PROJECTS
# with session-name->file/dir path
. /home/florian/bin/desktop/editor/defaults.sh

SELECTIONS=""
for project in "${!PROJECTS[@]}"; do
    SELECTIONS="$SELECTIONS\n$project"
done

SELECTION="$(echo -e "${SELECTIONS}" | dmenu -i -p "Project name:")"

emacs --maximized "${PROJECTS[$SELECTION]}"
